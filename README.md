# Zeknir Common
A general utilities and extension library for C# and Unity.

## Optional Components
Can be enabled by defining the following symbols.

- `ZEKNIR_ADDRESSABLES`: To enable extensions for the Unity Addressables package.