﻿using System.IO;
using UnityEditor.Experimental.AssetImporters;
using UnityEngine;

namespace Zeknir.Common.Editor.Serialization
{
    [ScriptedImporter(1, "savegame")]
    public class CustomTextAssetImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            var text = File.ReadAllText(ctx.assetPath);

            var fileObject = new TextAsset(text);

            ctx.AddObjectToAsset("Data", fileObject);
            ctx.SetMainObject(fileObject);
        }
    }
}