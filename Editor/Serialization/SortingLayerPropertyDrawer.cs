﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using Zeknir.Common.Unity.Editor;

namespace Zeknir.Common.Editor.Serialization
{
    [CustomPropertyDrawer(typeof(SortingLayerIdAttribute))]
    public class SortingLayerPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var layerId = property.intValue;

            var layers = SortingLayer.layers;
            var names = layers.Select(l => l.name).ToArray();
            if (!SortingLayer.IsValid(layerId))
                layerId = layers[0].id;

            var layerValue = SortingLayer.GetLayerValueFromID(layerId);
            var newLayerValue = EditorGUI.Popup(position, "Sorting Layer", layerValue, names);

            property.intValue = layers[newLayerValue].id;
        }
    }
}