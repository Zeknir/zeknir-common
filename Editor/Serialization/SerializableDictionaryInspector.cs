﻿using UnityEditor;
using UnityEngine;
using Zeknir.Common.Unity.Core;

namespace Zeknir.Common.Editor.Serialization
{
    [CustomPropertyDrawer(typeof(SerializableDictionary<,>))]
    public class SerializableDictionaryPropertyDrawer : PropertyDrawer
    {
        private const string itemPropertyName = "serializedItems";

        private static float lineHeight = EditorGUIUtility.singleLineHeight;
        private static float vertSpace = EditorGUIUtility.standardVerticalSpacing;
        private static float combinedPadding = lineHeight + vertSpace;

        public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
        {
            // Render list header and expand arrow.
            var list = property.FindPropertyRelative(itemPropertyName);
            var headerPos = new Rect(lineHeight, pos.y, pos.width, lineHeight);
            var fieldName = ObjectNames.NicifyVariableName(this.fieldInfo.Name);
            EditorGUI.PropertyField(headerPos, list, new GUIContent(fieldName));

            var e = Event.current;

            // Render list if expanded.
            var currentPos = new Rect(lineHeight, pos.y, pos.width, lineHeight);
            if (list.isExpanded)
            {
                // Render list size first.
                list.NextVisible(true);
                EditorGUI.indentLevel++;
                currentPos = new Rect(headerPos.x, headerPos.y + combinedPadding, pos.width, lineHeight);
                EditorGUI.PropertyField(currentPos, list, new GUIContent("Size"));

                // Render list content.
                var index = 0;
                currentPos.y += vertSpace;
                while (true)
                {
                    if (list.name == "Key" || list.name == "Value")
                    {
                        // Render key or value.
                        var entryPos = new Rect(currentPos.x, currentPos.y + combinedPadding, pos.width, lineHeight);
                        EditorGUI.PropertyField(entryPos, list, new GUIContent(list.name));

                        // Handle right click
                        if (e.type == EventType.MouseDown && e.button == 1 && entryPos.Contains(e.mousePosition))
                        {
                            var context = new GenericMenu();
                            var capturedIndex = index;
                            context.AddItem(new GUIContent("Remove Item"), false, () =>
                            {
                                var listContextual = property.FindPropertyRelative(itemPropertyName);
                                listContextual.DeleteArrayElementAtIndex(capturedIndex);
                                listContextual.serializedObject.ApplyModifiedProperties();
                            });
                            context.ShowAsContext();
                        }

                        currentPos.y += combinedPadding;

                        // Add spacing after each key value pair.
                        if (list.name == "Value")
                        {
                            currentPos.y += vertSpace;
                            index++;
                        }
                    }

                    if (!list.NextVisible(true)) break;
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var totHeight = 0f;

            // Return height of KeyValue list (take into account if list is expanded or not).
            var listProp = property.FindPropertyRelative(itemPropertyName);
            if (listProp.isExpanded)
            {
                listProp.NextVisible(true);
                var listSize = listProp.intValue;
                totHeight += listSize * 2f * combinedPadding + combinedPadding * 2f + listSize * vertSpace;
                return totHeight;
            }
            else
                return totHeight + lineHeight;
        }
    }
}