﻿using UnityEditor;
using UnityEditor.UI;
using Zeknir.Common.Unity.Ui.Components;

namespace Zeknir.Common.Editor.Ui.Components
{
    [CustomEditor(typeof(SyncableButton), true)]
    [CanEditMultipleObjects]
    public class SyncableButtonEditor : ButtonEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            this.serializedObject.Update();
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("SyncGroup"));
            this.serializedObject.ApplyModifiedProperties();
        }
    }
}