﻿using UnityEngine;

namespace Zeknir.Common.Mathematics
{
    public static class RectUtils
    {
        public static Rect FromPoints(Vector2 pos1, Vector2 pos2)
        {
            var minX = Mathf.Min(pos1.x, pos2.x);
            var maxX = Mathf.Max(pos1.x, pos2.x);
            var minY = Mathf.Min(pos1.y, pos2.y);
            var maxY = Mathf.Max(pos1.y, pos2.y);

            return Rect.MinMaxRect(minX, minY, maxX, maxY);
        }

        public static Rect FromCenter(Vector2 center, Vector2 size)
        {
            return new Rect(center - size / 2f, size);
        }

        public static Rect CorrectSize(this Rect rect)
        {
            var xMin = Mathf.Min(rect.xMin, rect.xMax);
            var xMax = Mathf.Max(rect.xMin, rect.xMax);
            var yMin = Mathf.Min(rect.yMin, rect.yMax);
            var yMax = Mathf.Max(rect.yMin, rect.yMax);

            return new Rect(rect.min, new Vector2(xMax - xMin, yMax - yMin));
        }

        public static Vector2 ClampPoint(this Rect rect, Vector2 point)
        {
            var x = Mathf.Clamp(point.x, rect.xMin, rect.xMax);
            var y = Mathf.Clamp(point.y, rect.yMin, rect.yMax);
            return new Vector2(x, y);
        }

        public static Rect ResizeFromCenter(this Rect rect, Vector2 newSize)
        {
            var sizeDiff = rect.size - newSize;
            var newPos = rect.position + sizeDiff / 2f;
            return new Rect(newPos, newSize);
        }

        public static Rect Encapsulate(this Rect baseRect, Rect otherRect)
        {
            var newXMax = Mathf.Max(baseRect.xMax, otherRect.xMax);
            var newXMin = Mathf.Min(baseRect.xMin, otherRect.xMin);
            var newYMax = Mathf.Max(baseRect.yMax, otherRect.yMax);
            var newYMin = Mathf.Min(baseRect.yMin, otherRect.yMin);

            return Rect.MinMaxRect(newXMin, newYMin, newXMax, newYMax);
        }
    }
}