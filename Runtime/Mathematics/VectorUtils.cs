﻿using System.Collections.Generic;
using UnityEngine;

namespace Zeknir.Common.Mathematics
{
    public static class VectorUtils
    {
        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            var sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            var cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            var tx = v.x;
            var ty = v.y;
            v.x = cos * tx - sin * ty;
            v.y = sin * tx + cos * ty;
            return v;
        }
        
        public static Vector2 SwapAxis(this Vector2 v)
        {
            return new Vector2(v.y, v.x);
        }

        public static Vector2 Mask(this Vector2 v, float? x = null, float? y = null)
        {
            return new Vector2(x ?? v.x, y ?? v.y);
        }

        public static Vector3 Mask(this Vector3 v, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x ?? v.x, y ?? v.y, z ?? v.z);
        }

        public static Vector3 ToVector3(this Vector2 vector2, float zValue = 0)
        {
            return new Vector3(vector2.x, vector2.y, zValue);
        }

        public static Vector2 Average(this IEnumerable<Vector2> enumerable)
        {
            var count = 0;
            var average = Vector2.zero;
            foreach (var v in enumerable)
            {
                average += v;
                count++;
            }

            return average / count;
        }

        public static Vector2 ToVector2(this float value)
        {
            return new Vector2(value, value);
        }

        public static Vector3 ToVector3(this float value)
        {
            return new Vector3(value, value, value);
        }      
        
        public static Vector2 ToVector2D(this Vector3 value)
        {
            return new Vector2(value.x, value.y);
        }
        
        public static Vector2Int ToVector2Int(this int value)
        {
            return new Vector2Int(value, value);
        }

        public static Vector3Int ToVector3Int(this int value)
        {
            return new Vector3Int(value, value, value);
        }
    }
}