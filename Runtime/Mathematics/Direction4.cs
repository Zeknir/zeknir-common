﻿using System;
using UnityEngine;

namespace Zeknir.Common.Mathematics
{
    public enum Direction4
    {
        Right,
        Up,
        Left,
        Down,
    }

    public static class Direction4Extensions
    {
        public static Vector2 ToVector2(this Direction4 direction)
        {
            switch (direction)
            {
                case Direction4.Right:
                    return Vector2.right;
                case Direction4.Up:
                    return Vector2.up;
                case Direction4.Left:
                    return Vector2.left;
                case Direction4.Down:
                    return Vector2.down;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public static Direction4 ToDirection4(this Vector2 vector)
        {
            var angle = Vector2.SignedAngle(Vector2.right, vector);
            return ToDirection4(angle);
        }

        public static Direction4 ToDirection4(this float euler)
        {
            if (euler < 0f)
                euler += Mathf.CeilToInt(euler / -360) * 360;
            else if (euler > 360f)
                euler %= 360;

            if (euler <= 45f)
                return Direction4.Right;
            else if (euler <= 135f)
                return Direction4.Up;
            else if (euler <= 225f)
                return Direction4.Left;
            else if (euler <= 315f)
                return Direction4.Down;
            else
                return Direction4.Right;
        }

        public static float ToEuler(this Direction4 direction)
        {
            switch (direction)
            {
                case Direction4.Right:
                    return 0f;
                case Direction4.Up:
                    return 90f;
                case Direction4.Left:
                    return 180f;
                case Direction4.Down:
                    return -90f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public static bool IsHorizontal(this Direction4 direction)
        {
            switch (direction)
            {
                case Direction4.Right:
                case Direction4.Left:
                    return true;
                case Direction4.Up:
                case Direction4.Down:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public static bool IsPositive(this Direction4 direction)
        {
            switch (direction)
            {
                case Direction4.Right:
                case Direction4.Up:
                    return true;
                case Direction4.Left:
                case Direction4.Down:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}