﻿using UnityEngine;

namespace Zeknir.Common.Mathematics
{
	public static class FloatUtils
	{
		public const float DefaultApproxThreshold = 0.0001f;

        public static bool ApproxEqual(this float a, float b, float threshold = DefaultApproxThreshold)
        {
            return Mathf.Abs(a - b) <= threshold;
        }
    }
}