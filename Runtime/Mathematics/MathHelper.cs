﻿using UnityEngine;

namespace Zeknir.Common.Mathematics
{
	public static class MathHelper
	{
        public static int ToInt(this float value)
        {
            return Mathf.RoundToInt(value);
        }
        
		public static Vector2 Max(Vector2 a, Vector2 b)
		{
			return new Vector2(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y));
		}

		public static Vector2Int Max(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y));
		}

		public static Vector3 Max(Vector3 a, Vector3 b)
		{
			return new Vector3(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y), Mathf.Max(a.z, b.z));
		}

		public static Vector3Int Max(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y), Mathf.Max(a.z, b.z));
		}

		public static Vector2 Min(Vector2 a, Vector2 b)
		{
			return new Vector2(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y));
		}

		public static Vector2Int Min(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y));
		}

		public static Vector3 Min(Vector3 a, Vector3 b)
		{
			return new Vector3(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y), Mathf.Min(a.z, b.z));
		}

		public static Vector3Int Min(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y), Mathf.Min(a.z, b.z));
		}

		public static int ModuloWrap(int number, int mod)
		{
			return (number % mod + mod) % mod;
		}
	}
}