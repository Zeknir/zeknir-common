﻿using System;
using System.IO;
using System.Linq;
using Zeknir.Logging;
using Zeknir.Common.Collections;

namespace Zeknir.Common.IO
{
    public static class IoUtils
    {
        public static void BackupAsRolling(string fileToBackup, bool useSubfolder)
        {
            var directoryPath = Path.GetDirectoryName(fileToBackup) ?? "";
            var fileWithoutExtension = Path.GetFileNameWithoutExtension(fileToBackup);
            var fileExtension = Path.GetExtension(fileToBackup);

            var backupFolder = directoryPath;
            if (useSubfolder)
            {
                backupFolder = Path.Combine(directoryPath, "Backups");
                if (!Directory.Exists(backupFolder))
                    Directory.CreateDirectory(backupFolder);
            }

            var backupFilePath = Path.Combine(backupFolder, $"{fileWithoutExtension}_Backup_{fileExtension}");

            var newFilePath = GetNewRollingFile(backupFilePath);

            Log.Write($"Backing up File \"{fileToBackup}\" to \"{newFilePath}\".");

            File.Move(fileToBackup, newFilePath);
        }

        public static string GetNewRollingFile(string baseFilePath, int maximumFilesToKeep = 5)
        {
            var directoryPath = Path.GetDirectoryName(baseFilePath);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(baseFilePath);
            var fileExtension = Path.GetExtension(baseFilePath);

            var directoryInfo = new DirectoryInfo(directoryPath);

            // Delete old Files
            var existingFiles = directoryInfo.GetFiles().Where(a => a.Name.StartsWith(fileNameWithoutExtension)).OrderBy(f => f.LastWriteTime).ToList();
            var filesToDelete = Math.Max(0, existingFiles.Count - (maximumFilesToKeep - 1));

            for (var i = 0; i < filesToDelete; i++)
                existingFiles[i].Delete();

            // Create File
            var rollingInfoString = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            var newFileName = $"{fileNameWithoutExtension}{rollingInfoString}{fileExtension}";
            var newFilePath = Path.Combine(directoryPath, newFileName);

            if (File.Exists(newFilePath))
                newFilePath = GetIncrementalFilePath(newFilePath);

            return newFilePath;
        }

        public static string GetIncrementalFilePath(string baseFilePath)
        {
            var directoryPath = Path.GetDirectoryName(baseFilePath);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(baseFilePath);
            var fileExtension = Path.GetExtension(baseFilePath);

            var files = Directory.GetFiles(directoryPath).ToHashSet();

            for (var i = 1; i < int.MaxValue; i++)
            {
                var fileNameToTest = buildFileName(i);

                if (files.Contains(fileNameToTest))
                    files.Remove(fileNameToTest);
                else
                    return fileNameToTest;
            }

            throw new IOException("Couldn't find incremental file name.");

            string buildFileName(int number)
            {
                return Path.Combine(directoryPath, fileNameWithoutExtension + number + fileExtension);
            }
        }
    }
}