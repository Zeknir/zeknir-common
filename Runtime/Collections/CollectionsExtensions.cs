﻿using System.Collections.Generic;

namespace Zeknir.Common.Collections
{
    public static class CollectionsExtensions
    {
        public static bool IsEmpty<T>(this Queue<T> queue)
        {
            return queue.Count == 0;
        }

        public static void Enqueue<T>(this Queue<T> queue, params T[] items)
        {
            foreach (var item in items)
                queue.Enqueue(item);
        }
    }
}