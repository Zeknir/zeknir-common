﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Zeknir.Common.Collections
{
    public sealed class ReadOnlySet<T> : IEnumerable<T>, IReadOnlyCollection<T>
    {
        private readonly HashSet<T> internalSet;

        public int Count => this.internalSet?.Count ?? 0;

        public ReadOnlySet(HashSet<T> set)
        {
            this.internalSet = set;
        }

        public bool Contains(T item)
        {
            return this.internalSet?.Contains(item) ?? false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.internalSet?.GetEnumerator() ?? Enumerable.Empty<T>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}