﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zeknir.Common.Collections
{
    public static class EnumerableUtils
    {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)
        {
            return new HashSet<T>(enumerable);
        }

        public static Queue<T> ToQueue<T>(this IEnumerable<T> enumerable)
        {
            return new Queue<T>(enumerable);
        }

        public static Stack<T> ToStack<T>(this IEnumerable<T> enumerable)
        {
            return new Stack<T>(enumerable);
        }

        public static bool CountAtLeast<T>(this IEnumerable<T> enumerable, int minimumItemCount, Predicate<T> predicate = null)
        {
            var counter = 0;
            foreach (var item in enumerable)
            {
                if (predicate != null && !predicate(item))
                    continue;

                counter++;
                if (counter >= minimumItemCount)
                    return true;
            }

            return false;
        }

        public static IEnumerable<T> Without<T>(this IEnumerable<T> enumerable, T ignore)
        {
            return enumerable.Where(item => !Equals(item, ignore));
        }

        public static IEnumerable<T> AsEnumerable<T>(this T obj)
        {
            yield return obj;
        }

        public static IEnumerable<T> EnumerateLinked<T>(T first, Func<T, T> selector, Predicate<T> predicate = null)
        {
            var duplicateSet = new HashSet<T>();

            if (predicate == null)
                predicate = a => !Equals(a, default);

            var element = first;
            while (predicate(element))
            {
                if (duplicateSet.Contains(element))
                    throw new ArgumentException("Detected self-referencing loop. Can't enumerate linked elements.");

                duplicateSet.Add(element);
                var nextElement = selector(element);

                yield return element;

                element = nextElement;
            }
        }
    }
}