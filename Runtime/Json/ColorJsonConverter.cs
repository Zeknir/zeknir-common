﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using Zeknir.Common.Colors;
using Zeknir.Common.Core;

namespace Zeknir.Common.Json
{
    [JsonSystem.GlobalJsonConverterAttribute]
    public class ColorJsonConverter : JsonConverter<Color>
    {
        public override void WriteJson(JsonWriter writer, Color value, JsonSerializer serializer)
        {
            var colorString = $"#{value.ToHex()}";
            writer.WriteValue(colorString);
        }

        public override Color ReadJson(JsonReader reader, Type objectType, Color existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var value = reader.Value.As<string>();
            return ColorUtils.FromHex(value);
        }
    }
}