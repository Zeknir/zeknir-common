﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Zeknir.Common.Reflection;

namespace Zeknir.Common.Json
{
    public class ComplexDictionaryJsonSerializer : JsonConverter
    {
        private const string keyPropertyName = "Key";
        private const string valuePropertyName = "Value";
        
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var dict = (IDictionary) value;

            var targetArray = new JArray();
            
            foreach (var key in dict.Keys)
            {
                var keyValue = dict[key];

                var targetValuePair = new JObject
                {
                    {keyPropertyName, JToken.FromObject(key, serializer)},
                    {valuePropertyName, JToken.FromObject(keyValue, serializer)}
                };
                
                targetArray.Add(targetValuePair);
            }
            
            targetArray.WriteTo(writer, serializer.Converters.ToArray());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var keyType = objectType.GenericTypeArguments[0];
            var valueType = objectType.GenericTypeArguments[1];
            
            var array = JArray.Load(reader);

            var dict = (IDictionary)Activator.CreateInstance(objectType);
            
            foreach (var token in array)
            {
                var key = token[keyPropertyName]?.ToObject(keyType, serializer) ?? throw new JsonSerializationException("Cannot create Dictionary with null key.");
                var value = token[valuePropertyName]?.ToObject(valueType, serializer);
                
                dict.Add(key, value);
            }

            return dict;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsTypeAnyGenericOf(typeof(Dictionary<,>));
        }
    }
}