﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Zeknir.Common.Reflection;

namespace Zeknir.Common.Json
{
    public static class JsonSystem
    {
        private static List<JsonConverter> globalConverters = new List<JsonConverter>();

        public static void Init()
        {
            var globalConverterTypes = typeof(JsonConverter).GetDerivedTypes().Where(t => t.HasCustomAttribute<GlobalJsonConverterAttribute>());

            foreach (var converter in globalConverterTypes)
            {
                if (converter.GetConstructor(Type.EmptyTypes) != null)
                    globalConverters.Add((JsonConverter) Activator.CreateInstance(converter));
            }
        }

        public static JsonSerializerSettings GetDefaultJsonSettings()
        {
            return new JsonSerializerSettings
            {
                Converters = globalConverters
            };
        }

        [AttributeUsage(AttributeTargets.Class, Inherited = false)]
        public sealed class GlobalJsonConverterAttribute : Attribute { }
    }
}