﻿using System;
using System.Globalization;
using UnityEngine;

namespace Zeknir.Common.Colors
{
    public static class ColorUtils
    {
        public static Color Transparent => new Color(0f, 0f, 0f, 0f);
        public static Color Black => new Color(0f, 0f, 0f);
        public static Color White => new Color(1f, 1f, 1f);
        public static Color DarkGray => new Color(0.25f, 0.25f, 0.25f);
        public static Color Gray => new Color(0.5f, 0.5f, 0.5f);
        public static Color LightGray => new Color(0.75f, 0.75f, 0.75f);
        public static Color Red => new Color(1f, 0f, 0f);
        public static Color Orange => new Color(1f, 0.5f, 0f);
        public static Color Brown => new Color(0.5f, 0.25f, 0f);
        public static Color Yellow => new Color(1f, 1f, 0f);
        public static Color Citrus => new Color(0.5f, 1f, 0f);
        public static Color Green => new Color(0f, 1f, 0f);
        public static Color AquaGreen => new Color(0f, 1f, 0.5f);
        public static Color Cyan => new Color(0f, 1f, 1f);
        public static Color SkyBlue => new Color(0f, 0.5f, 1f);
        public static Color Blue => new Color(0f, 0f, 1f);
        public static Color Purple => new Color(0.5f, 0f, 1f);
        public static Color Pink => new Color(1f, 0f, 1f);
        public static Color HotPink => new Color(1f, 0f, 0.5f);

        public static Color Pale(this Color color)
        {
            return new Color(PaleValue(color.r), PaleValue(color.g), PaleValue(color.b));

            float PaleValue(float value)
            {
                return 1f - (1f - value) / 2f;
            }
        }

        public static Color Darken(this Color color)
        {
            return new Color(DarkenValue(color.r), DarkenValue(color.g), DarkenValue(color.b));

            float DarkenValue(float value)
            {
                return value / 2f;
            }
        }

        public static Color Mask(this Color c, float? r = null, float? g = null, float? b = null, float? a = null)
        {
            return new Color(r ?? c.r, g ?? c.g, b ?? c.b, a ?? c.a);
        }

        public static string ToHex(this Color c, bool includeAlpha = true)
        {
            var baseString = rgbToInt(c.r).ToString("X2") + rgbToInt(c.g).ToString("X2") + rgbToInt(c.b).ToString("X2");

            if (includeAlpha)
                baseString += rgbToInt(c.a).ToString("X2");

            return baseString;

            int rgbToInt(float value)
            {
                return Mathf.RoundToInt(value * 255);
            }
        }

        public static Color FromHex(string text)
        {
            if (text.StartsWith("#"))
                text = text.Substring(1);

            if (!(text.Length == 6 || text.Length == 8))
                throw new ArgumentException("Invalid Hex Color Format.");

            var r = int.Parse(text.Substring(0, 2), NumberStyles.HexNumber);
            var g = int.Parse(text.Substring(2, 2), NumberStyles.HexNumber);
            var b = int.Parse(text.Substring(4, 2), NumberStyles.HexNumber);
            var a = 255;

            if (text.Length == 8)
                a = int.Parse(text.Substring(6, 2), NumberStyles.HexNumber);

            return new Color(intToRgb(r), intToRgb(g), intToRgb(b), intToRgb(a));

            float intToRgb(int value)
            {
                return value / 255.0f;
            }
        }
    }
}