﻿using System;
using UnityEngine;

namespace Zeknir.Common.Colors
{
	public readonly struct HslColor
	{
		public readonly float Hue;
		public readonly float Saturation;
		public readonly float Lightness;
		public readonly float Alpha;

		public HslColor(int hue, float saturation, float lightness, float alpha = 1f) : this(hue / 360f, saturation, lightness, alpha) { }

		public HslColor(float hue, float saturation, float lightness, float alpha = 1f)
		{
			this.Hue = hue;
			this.Saturation = saturation;
			this.Lightness = lightness;
			this.Alpha = alpha;
		}

		public static implicit operator Color(HslColor hsl)
		{
			float r, g, b;

			if (hsl.Saturation == 0.0f)
			{
				r = g = b = hsl.Lightness;
			}

			else
			{
				var q = hsl.Lightness < 0.5f ? hsl.Lightness * (1.0f + hsl.Saturation) : hsl.Lightness + hsl.Saturation - hsl.Lightness * hsl.Saturation;
				var p = 2.0f * hsl.Lightness - q;
				r = HueToRgb(p, q, hsl.Hue + 1.0f / 3.0f);
				g = HueToRgb(p, q, hsl.Hue);
				b = HueToRgb(p, q, hsl.Hue - 1.0f / 3.0f);
			}

			return new Color(r, g, b, hsl.Alpha);

			// Helper for HslToRgba
			float HueToRgb(float p, float q, float t)
			{
				if (t < 0.0f)
				{
					t += 1.0f;
				}

				if (t > 1.0f)
				{
					t -= 1.0f;
				}

				if (t < 1.0f / 6.0f)
				{
					return p + (q - p) * 6.0f * t;
				}

				if (t < 1.0f / 2.0f)
				{
					return q;
				}

				if (t < 2.0f / 3.0f)
				{
					return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
				}

				return p;
			}
		}

		public static explicit operator HslColor(Color rgba)
		{
			var max = Math.Max(rgba.r, Math.Max(rgba.g, rgba.b));
			var min = Math.Min(rgba.r, Math.Min(rgba.g, rgba.b));

			float h, s, l;
			l = (max + min) / 2.0f;

			if (max == min)
			{
				h = s = 0.0f;
			}
			else
			{
				var d = max - min;
				s = l > 0.5f ? d / (2.0f - max - min) : d / (max + min);

				if (rgba.r > rgba.g && rgba.r > rgba.b)
				{
					h = (rgba.g - rgba.b) / d + (rgba.g < rgba.b ? 6.0f : 0.0f);
				}

				else if (rgba.g > rgba.b)
				{
					h = (rgba.b - rgba.r) / d + 2.0f;
				}

				else
				{
					h = (rgba.r - rgba.g) / d + 4.0f;
				}

				h /= 6.0f;
			}

			return new HslColor(h, s, l, rgba.a);
		}
	}
}