﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Zeknir.Common.Json;
using Zeknir.Common.Reflection;

namespace Zeknir.Common.Versioning
{
    public class VersionedObject<TObject> where TObject : class, IVersioned, new()
    {
        public VersionedObjectStatus Status { get; private set; }
        public Exception Error { get; private set; }
        public TObject Value { get; private set; }
        public Version ExpectedVersion { get; private set; }

        private JObject jsonData;
        private VersionUpgradePath<TObject> upgradePath;

        protected VersionedObject() { }

        public VersionedObject<TObject> Upgrade()
        {
            if (!(this.upgradePath != null && this.Status == VersionedObjectStatus.Upgradeable))
                throw new InvalidOperationException("Versioned Object not upgradable.");

            this.upgradePath.Upgrade(this, ref this.jsonData);
            return Open(this.jsonData, this.ExpectedVersion);
        }

        public string Serialize()
        {
            if (this.Value == null || this.Status != VersionedObjectStatus.Ready)
                throw new InvalidOperationException("Can't serialize unloaded object.");

            this.Value.OnSerializing();

            var jsonObject = new JObject();
            jsonObject.Add("Version", JObject.FromObject(this.Value.Version));
            foreach (var property in typeof(TObject).GetProperties())
            {
                if (!property.PropertyType.IsTypeAnyGenericOf(typeof(VersionedContainer<>)))
                    continue;

                var versionedPropertyAttribute = property.GetCustomAttributes(typeof(VersionedPropertyAttribute), false).FirstOrDefault() as VersionedPropertyAttribute;
                if (versionedPropertyAttribute == null)
                    continue;

                var versionedContainer = (VersionedContainer) property.GetValue(this.Value);
                jsonObject.Add(property.Name, JObject.FromObject(versionedContainer.Value, JsonSerializer.Create(JsonSystem.GetDefaultJsonSettings())));
            }

            return jsonObject.ToString(Formatting.Indented);
        }

        public static VersionedObject<TObject> Create(TObject data)
        {
            return new VersionedObject<TObject>()
            {
                Status = VersionedObjectStatus.Ready,
                Value = data,
                ExpectedVersion = data?.Version ?? Version.Zero
            };
        }

        public static VersionedObject<TObject> Open(string jsonSource, Version expectedVersion)
        {
            try
            {
                var jsonObject = JObject.Parse(jsonSource);
                return Open(jsonObject, expectedVersion);
            }
            catch (Exception ex)
            {
                return new VersionedObject<TObject>()
                {
                    Status = VersionedObjectStatus.Invalid,
                    Error = ex,
                };
            }
        }

        public static VersionedObject<TObject> Open(JObject jsonObject, Version expectedVersion)
        {
            try
            {
                if (jsonObject == null)
                    throw new InvalidDataException("Invalid data.");

                // Read JSON
                var objectVersion = jsonObject["Version"]?.ToObject<Version>();

                // Pre Checks
                if (!objectVersion.HasValue)
                    throw new InvalidDataException("No version info found in file.");

                if (objectVersion > expectedVersion)
                    return new VersionedObject<TObject>() {Status = VersionedObjectStatus.TooNew};

                // Create
                var versionedObject = new VersionedObject<TObject>
                {
                    jsonData = jsonObject,
                    Status = VersionedObjectStatus.Ready,
                    ExpectedVersion = expectedVersion,
                    Value = new TObject()
                    {
                        Version = objectVersion.Value
                    }
                };

                // Create Containers
                var anyTooOld = false;
                expectedVersion.CompareTo(objectVersion.Value, out var isOldMajor, out var isOldMinor, out var isOldPatch);

                foreach (var property in typeof(TObject).GetProperties())
                {
                    if (!property.PropertyType.IsTypeAnyGenericOf(typeof(VersionedContainer<>)))
                        continue;

                    var versionedPropertyAttribute = property.GetCustomAttributes(typeof(VersionedPropertyAttribute), false).FirstOrDefault() as VersionedPropertyAttribute;
                    if (versionedPropertyAttribute == null)
                        continue;

                    var isContainerTooOld = false;
                    switch (versionedPropertyAttribute.Type)
                    {
                        case VersionedPropertyType.AlwaysValid:
                            isContainerTooOld = false;
                            break;
                        case VersionedPropertyType.InvalidAfterMajorIncrease:
                            isContainerTooOld = isOldMajor;
                            break;
                        case VersionedPropertyType.InvalidAfterMinorIncrease:
                            isContainerTooOld = isOldMinor;
                            break;
                        case VersionedPropertyType.InvalidAfterPatchIncrease:
                            isContainerTooOld = isOldPatch;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    object container;
                    if (isContainerTooOld)
                    {
                        anyTooOld = true;
                        container = Activator.CreateInstance(property.PropertyType, VersionedContainerStatus.ToOld, null);
                    }
                    else
                    {
                        var data = jsonObject[property.Name];

                        if (data == null)
                            container = Activator.CreateInstance(property.PropertyType, VersionedContainerStatus.Invalid, null);
                        else
                            container = Activator.CreateInstance(property.PropertyType, VersionedContainerStatus.Unloaded, data);
                    }

                    property.SetValue(versionedObject.Value, container);
                }

                // Check Upgrade Path
                if (anyTooOld)
                {
                    versionedObject.upgradePath = VersionUpgrader<TObject>.FindUpgradePath(versionedObject, expectedVersion);
                    var hasUpgradePath = versionedObject.upgradePath != null;

                    versionedObject.Status = hasUpgradePath ? VersionedObjectStatus.Upgradeable : VersionedObjectStatus.TooOld;
                }

                return versionedObject;
            }
            catch (Exception ex)
            {
                return new VersionedObject<TObject>()
                {
                    Status = VersionedObjectStatus.Invalid,
                    Error = ex,
                };
            }
        }
    }
}