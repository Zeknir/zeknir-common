﻿namespace Zeknir.Common.Versioning
{
    public enum VersionedObjectStatus
    {
        Invalid,
        TooNew,
        TooOld,
        Upgradeable,
        Ready,
    }
    
    public enum VersionedContainerStatus
    {
        Invalid,
        ToOld,
        Unloaded,
        Loaded,
    }
}