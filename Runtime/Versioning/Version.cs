﻿using System;

namespace Zeknir.Common.Versioning
{
    [Serializable]
    public struct Version : IComparable<Version>
    {
        public static readonly Version Zero = new Version(0, 0);

        public int MajorVersion;
        public int MinorVersion;
        public int PatchVersion;

        public Version(int majorVersion, int minorVersion, int patchVersion = 0)
        {
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;
            this.PatchVersion = patchVersion;
        }

        public bool Equals(Version other)
        {
            return this.MajorVersion == other.MajorVersion && this.MinorVersion == other.MinorVersion && this.PatchVersion == other.PatchVersion;
        }

        public override bool Equals(object obj)
        {
            return obj is Version other && this.Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.MajorVersion;
                hashCode = (hashCode * 397) ^ this.MinorVersion;
                hashCode = (hashCode * 397) ^ this.PatchVersion;
                return hashCode;
            }
        }

        public int CompareTo(Version other)
        {
            var majorVersionComparison = this.MajorVersion.CompareTo(other.MajorVersion);
            if (majorVersionComparison != 0)
                return majorVersionComparison;

            var minorVersionComparison = this.MinorVersion.CompareTo(other.MinorVersion);
            if (minorVersionComparison != 0)
                return minorVersionComparison;

            return this.PatchVersion.CompareTo(other.PatchVersion);
        }

        public override string ToString()
        {
            return $"{this.MajorVersion}.{this.MinorVersion}.{this.PatchVersion}";
        }

        public static bool operator ==(Version a, Version b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Version a, Version b)
        {
            return !a.Equals(b);
        }

        public static bool operator >(Version a, Version b)
        {
            return a.CompareTo(b) > 0;
        }

        public static bool operator <(Version a, Version b)
        {
            return a.CompareTo(b) < 0;
        }

        public static bool operator >=(Version a, Version b)
        {
            return a.CompareTo(b) >= 0;
        }

        public static bool operator <=(Version a, Version b)
        {
            return a.CompareTo(b) <= 0;
        }

        public void CompareTo(Version other, out bool majorIncreased, out bool minorIncreased, out bool patchIncreased)
        {
            majorIncreased = minorIncreased = patchIncreased = false;

            if (this.MajorVersion > other.MajorVersion)
            {
                majorIncreased = minorIncreased = patchIncreased = true;
                return;
            }    
            
            if (this.MinorVersion > other.MinorVersion)
            {
                minorIncreased = patchIncreased = true;
                return;
            }    
            
            if (this.PatchVersion > other.PatchVersion)
            {
                patchIncreased = true;
                return;
            }
        }
    }
}