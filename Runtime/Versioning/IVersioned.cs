﻿namespace Zeknir.Common.Versioning
{
    public interface IVersioned
    {
        Version Version { get; set; }

        void OnSerializing();
    }
}