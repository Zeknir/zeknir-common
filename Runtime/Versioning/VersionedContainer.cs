﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Zeknir.Common.Versioning
{
    public class VersionedContainer<T> : VersionedContainer
    {
        public new T Value => (T) base.Value;
        protected override Type ValueType => typeof(T);

        public VersionedContainer(VersionedContainerStatus status, JToken data) : base(status, data) { }
        public VersionedContainer(T data) : base(data) { }
    }

    public abstract class VersionedContainer
    {
        public VersionedContainerStatus Status { get; private set; }
        public Exception Error { get; private set; }
        public object Value { get; private set; }

        public bool IsLoaded => this.Status == VersionedContainerStatus.Loaded;

        protected abstract Type ValueType { get; }

        private JToken rawData;

        public VersionedContainer(VersionedContainerStatus status, JToken data)
        {
            this.Status = status;
            this.rawData = data;
        }

        public VersionedContainer(object data)
        {
            this.Value = data;
            this.Status = VersionedContainerStatus.Loaded;
        }

        public void Load(object deserializationContext = null)
        {
            try
            {
                if (this.Status == VersionedContainerStatus.Invalid)
                    throw new InvalidOperationException("Data invalid.");

                if (this.Status == VersionedContainerStatus.Loaded)
                    return;

                this.Value = this.rawData.ToObject(this.ValueType, JsonSerializer.CreateDefault(new JsonSerializerSettings() {Context = new StreamingContext(StreamingContextStates.All, deserializationContext)}));
                this.Status = VersionedContainerStatus.Loaded;
                this.rawData = null;
            }
            catch (Exception ex)
            {
                this.Error = ex;
                this.Status = VersionedContainerStatus.Invalid;
                throw;
            }
        }
    }
}