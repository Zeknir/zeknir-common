﻿using System;

namespace Zeknir.Common.Versioning
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class VersionedPropertyAttribute : Attribute
    {
        public readonly VersionedPropertyType Type;

        public VersionedPropertyAttribute(VersionedPropertyType type = VersionedPropertyType.InvalidAfterMinorIncrease)
        {
            this.Type = type;
        }
    }
}