﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Zeknir.Common.Collections;
using Zeknir.Common.Reflection;

namespace Zeknir.Common.Versioning
{
    public abstract class VersionUpgrader<TObject> where TObject : class, IVersioned, new()
    {
        public abstract bool CanUpgrade(VersionedObject<TObject> versionedObject);
        public abstract Version TargetVersion { get; }
        public abstract void Upgrade(VersionedObject<TObject> versionedObject, ref JObject jsonData);

        public static VersionUpgradePath<TObject> FindUpgradePath(VersionedObject<TObject> versionedObject, Version finalTargetVersion)
        {
            var upgraders = typeof(VersionUpgrader<TObject>).GetDerivedTypes().Select(Activator.CreateInstance).Cast<VersionUpgrader<TObject>>().ToList();

            var upgradePath = new VersionUpgradePath<TObject>();
            var hasFoundPath = findNextUpgrader(upgradePath, versionedObject, upgraders, finalTargetVersion);

            return hasFoundPath ? upgradePath : null;
        }

        private static bool findNextUpgrader(VersionUpgradePath<TObject> upgradePath, VersionedObject<TObject> versionedObject, IEnumerable<VersionUpgrader<TObject>> upgraders, Version finalTargetVersion)
        {
            var originalVersion = versionedObject.Value.Version;

            foreach (var upgrader in upgraders)
            {
                if (upgrader.CanUpgrade(versionedObject) && upgrader.TargetVersion > originalVersion && upgrader.TargetVersion <= finalTargetVersion)
                {
                    versionedObject.Value.Version = upgrader.TargetVersion;

                    if (versionedObject.Value.Version == finalTargetVersion || findNextUpgrader(upgradePath, versionedObject, upgraders.Without(upgrader), finalTargetVersion))
                    {
                        upgradePath.Upgraders.Push(upgrader);
                        versionedObject.Value.Version = originalVersion;
                        return true;
                    }
                }
            }

            versionedObject.Value.Version = originalVersion;
            return false;
        }
    }
}