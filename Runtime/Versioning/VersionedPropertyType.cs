﻿namespace Zeknir.Common.Versioning
{
    public enum VersionedPropertyType
    {
        AlwaysValid,
        InvalidAfterMajorIncrease,
        InvalidAfterMinorIncrease,
        InvalidAfterPatchIncrease,
    }
}