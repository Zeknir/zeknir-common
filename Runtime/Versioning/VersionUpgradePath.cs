﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Zeknir.Common.Versioning
{
    public class VersionUpgradePath<TObject> where TObject : class, IVersioned, new()
    {
        public Stack<VersionUpgrader<TObject>> Upgraders { get; } = new Stack<VersionUpgrader<TObject>>();

        public void Upgrade(VersionedObject<TObject> versionedObject, ref JObject jsonData)
        {
            foreach (var upgrader in this.Upgraders)
                upgrader.Upgrade(versionedObject, ref jsonData);
        }
    }
}