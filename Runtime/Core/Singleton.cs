﻿using System;

namespace Zeknir.Common.Core
{
    public abstract class Singleton<T> where T : Singleton<T>
    {
        public static T Instance { get; private set; }

        public static void DisposeInstance()
        {
            Instance = null;
        }
        
        protected Singleton()
        {
            if (Instance != null)
                throw new InvalidOperationException("Singleton instance hasn't been removed yet.");
            
            Instance = (T) this;
        }
    }
}