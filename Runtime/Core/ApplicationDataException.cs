﻿using System;

namespace Zeknir.Common.Core
{
    public class ApplicationDataException : Exception
    {
        public ApplicationDataException(string message) : base(message) { }

        public ApplicationDataException(string message, Exception innerException) : base(message, innerException) { }
    }
}