﻿namespace Zeknir.Common.Core
{
    public delegate void SimpleEventHandler<in T>(T sender);

    public delegate void SimpleEventHandler<in TSender, in TValue>(TSender sender, TValue value);
}