﻿using System;

namespace Zeknir.Common.Core
{
    public static class CoreHelper
    {
        public static T As<T>(this object obj)
        {
            return (T) obj;
        }
        
        public static T Apply<T>(this T obj, Action<T> action)
        {
            action(obj);
            return obj;
        }
    }
}