﻿using System;

namespace Zeknir.Common.Core
{
    public class Randomizer
    {
        #region Static

        public static readonly Randomizer Global;

        static Randomizer()
        {
            Global = new Randomizer();
        }

        #endregion


        private readonly Random random;

        public Randomizer()
        {
            this.random = Global != null ? new Random(Global.NextInt()) : new Random();
        }

        public double Next0To1()
        {
            return this.random.NextDouble();
        }

        public int NextInt()
        {
            return this.random.Next();
        }

        public int NextInt(int exclMax)
        {
            return this.random.Next(exclMax);
        }

        public int NextInt(int inclMin, int exclMax)
        {
            return this.random.Next(inclMin, exclMax);
        }

        public float NextFloat(int inclMin = 0, int exclMax = 1)
        {
            return (float) (this.Next0To1() * (exclMax - inclMin) + inclMin);
        }

        public bool NextBool(float chance = 0.5f)
        {
            if (chance >= 1f)
                return true;

            if (chance <= 0f)
                return false;

            return this.Next0To1() < chance;
        }
    }
}