﻿namespace Zeknir.Common.Core
{
    public static class StringHelper
    {
        public static string SubstringTo(this string text, int startIndex, int exclusiveEndIndex)
        {
            var length = exclusiveEndIndex - startIndex;
            return text.Substring(startIndex, length);
        }

        public static string Replace(this string text, int startIndex, int exclusiveEndIndex, string insertValue)
        {
            return text.Remove(startIndex, exclusiveEndIndex - startIndex).Insert(startIndex, insertValue);
        }
    }
}