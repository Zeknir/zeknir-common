﻿namespace Zeknir.Common.Core
{
	public delegate void EventPropertyChanged<in T>(T newValue);

	public class EventProperty<T>
	{
		public event EventPropertyChanged<T> ValueChanged;

		private T value;
		public T Value
		{
			get => this.value;
			set
			{
				this.value = value;
				this.ValueChanged?.Invoke(value);
			}
		}

		public EventProperty(T value = default)
		{
			this.Value = value;
		}

		public static implicit operator T(EventProperty<T> eventProperty)
		{
			return eventProperty.Value;
		}
	}
}