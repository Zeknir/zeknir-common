﻿using System;
using System.Collections.Generic;

namespace Zeknir.Common.Core
{
    public class RegisterFlag
    {
        public event EventPropertyChanged<bool> ValueChanged;

        public bool Value => this.behavior == RegisterFlagBehavior.TrueIfAny ? this.objects.Count > 0 : this.objects.Count <= 0;

        private readonly RegisterFlagBehavior behavior;
        private readonly HashSet<object> objects = new HashSet<object>();

        public RegisterFlag(RegisterFlagBehavior behavior)
        {
            this.behavior = behavior;
        }

        public void Register(object obj)
        {
            var oldValue = this.Value;
            this.objects.Add(obj);

            if (oldValue != this.Value)
                this.ValueChanged?.Invoke(this.Value);
        }

        public void Deregister(object obj)
        {
            var oldValue = this.Value;
            this.objects.Remove(obj);

            if (oldValue != this.Value)
                this.ValueChanged?.Invoke(this.Value);
        }

        public IDisposable Using()
        {
            return new RegisterFlagUsing(this);
        }

        private class RegisterFlagUsing : IDisposable
        {
            private readonly RegisterFlag flag;

            public RegisterFlagUsing(RegisterFlag flag)
            {
                this.flag = flag;
                this.flag.Register(this);
            }

            public void Dispose()
            {
                this.flag.Deregister(this);
            }
        }
    }

    public enum RegisterFlagBehavior
    {
        TrueIfAny,
        TrueIfNone
    }
}