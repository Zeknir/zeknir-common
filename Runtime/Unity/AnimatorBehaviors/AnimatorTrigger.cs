﻿using UnityEngine;

namespace Zeknir.Common.Unity.AnimatorBehaviors
{
	public delegate void AnimationTriggerEvent();

	public class AnimatorTrigger : StateMachineBehaviour
	{
		public string TriggerName;
		public AnimatorTriggerType TriggerType;

		public event AnimationTriggerEvent OnTrigger;

		private bool triggeredFinished;

		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (this.TriggerType == AnimatorTriggerType.OnEnter)
			{
				this.OnTrigger?.Invoke();
			}

			this.triggeredFinished = false;
		}

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (this.TriggerType == AnimatorTriggerType.OnUpdate)
			{
				this.OnTrigger?.Invoke();
			}

			if (this.TriggerType == AnimatorTriggerType.OnFinishedPlaying)
			{
				if (!this.triggeredFinished && stateInfo.normalizedTime >= 1f)
				{
					this.OnTrigger?.Invoke();
					this.triggeredFinished = true;
				}
			}
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (this.TriggerType == AnimatorTriggerType.OnFinishedPlaying)
			{
				if (!this.triggeredFinished && stateInfo.normalizedTime >= 1f)
				{
					this.OnTrigger?.Invoke();
					this.triggeredFinished = true;
				}
			}

			if (this.TriggerType == AnimatorTriggerType.OnExit)
			{
				this.OnTrigger?.Invoke();
			}
		}
	}

	public enum AnimatorTriggerType
	{
		OnEnter,
		OnExit,
		OnUpdate,
		OnFinishedPlaying,
	}
}