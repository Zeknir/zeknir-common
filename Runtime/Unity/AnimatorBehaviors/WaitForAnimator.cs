﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zeknir.Logging;

namespace Zeknir.Common.Unity.AnimatorBehaviors
{
	public class WaitForAnimator : CustomYieldInstruction
	{
		private bool hasAnimatorTriggered = false;
		private List<AnimatorTrigger> triggers;

		public WaitForAnimator(Animator animator, string triggerName)
		{
			var behaviors = animator.GetBehaviours<AnimatorTrigger>();
			this.triggers = behaviors.Where(b => b.TriggerName == triggerName).ToList();

			if (this.triggers.Count <= 0)
			{
				Log.Write($"Failed to find trigger \"{triggerName}\". Skipping wait...", LogLevel.Error);
				this.hasAnimatorTriggered = true;
				return;
			}

			foreach (var trigger in this.triggers)
				trigger.OnTrigger += this.handleOnAnimatorTrigger;
		}

		private void handleOnAnimatorTrigger()
		{
			this.hasAnimatorTriggered = true;

			foreach (var trigger in this.triggers)
				trigger.OnTrigger -= this.handleOnAnimatorTrigger;
		}

		public override bool keepWaiting => !this.hasAnimatorTriggered;
	}
}