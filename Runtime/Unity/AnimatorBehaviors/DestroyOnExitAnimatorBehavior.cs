﻿using UnityEngine;

namespace Zeknir.Common.Unity.AnimatorBehaviors
{
	public class DestroyOnExitAnimatorBehavior : StateMachineBehaviour
	{
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			Destroy(animator.gameObject);
		}
	}
}