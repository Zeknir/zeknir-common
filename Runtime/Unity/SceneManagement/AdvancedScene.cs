﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zeknir.Common.Unity.SceneManagement
{
	public class AdvancedScene : AdvancedSceneReference
	{
		public bool Active
		{
			get => this.active;
			set => this.setActive(value);
		}

		private bool active = true;
		private HashSet<AdvancedScene> disablingScenes = new HashSet<AdvancedScene>();
		private HashSet<AdvancedScene> disabledByScenes = new HashSet<AdvancedScene>();

		private List<GameObject> gameObjectsDisabledByScene = new List<GameObject>();

		public AdvancedScene(int sceneIndex, int priority) : base(sceneIndex, priority) { }
		public AdvancedScene(Scene scene, int priority) : base(scene, priority) { }

		public void DisableOtherScene(AdvancedScene sceneToDisable)
		{
			this.disablingScenes.Add(sceneToDisable);
			sceneToDisable.disabledByScenes.Add(this);
			sceneToDisable.Active = false;
		}

		public void EnableOtherScene(AdvancedScene sceneToEnable)
		{
			this.disablingScenes.Remove(sceneToEnable);
			sceneToEnable.disabledByScenes.Remove(this);
			if (sceneToEnable.disabledByScenes.Count <= 0)
			{
				sceneToEnable.Active = true;
			}
		}

		public void EnableAllOtherScenes()
		{
			foreach (var disabledScene in this.disablingScenes.ToList())
				this.EnableOtherScene(disabledScene);
		}

		private void setActive(bool newValue)
		{
			if (this.active == newValue) // Ignore if no change
			{
				return;
			}

			this.active = newValue;
			if (newValue) // Restore Scene
			{
				foreach (var obj in this.gameObjectsDisabledByScene)
					obj.SetActive(true);
				this.gameObjectsDisabledByScene.Clear();
			}
			else // Disable Scene
			{
				foreach (var obj in this.Scene.GetRootGameObjects())
				{
					if (obj.activeSelf)
					{
						obj.SetActive(false);
						this.gameObjectsDisabledByScene.Add(obj);
					}
				}
			}
		}

		public override string ToString()
		{
			return $"({this.Scene.name ?? "?"}: {this.SceneIndex}, {this.Active})";
		}
	}
}