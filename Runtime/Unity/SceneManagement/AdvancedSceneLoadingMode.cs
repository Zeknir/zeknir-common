﻿using System.Collections.Generic;
using System.Linq;

namespace Zeknir.Common.Unity.SceneManagement
{
	public enum AdvancedSceneLoadingMode
	{
		Additional,
		DestroyAllOther,
		DestroyLower,
		DestroyLowerOrEqual,
		DisableAllOther,
		DisableLower,
		DisableLowerOrEqual,
	}

	public static class AdvancedSceneLoadingModeExtensions
	{
		public static bool IsDestroy(this AdvancedSceneLoadingMode mode)
		{
			switch (mode)
			{
				case AdvancedSceneLoadingMode.DestroyAllOther:
				case AdvancedSceneLoadingMode.DestroyLower:
				case AdvancedSceneLoadingMode.DestroyLowerOrEqual:
					return true;
				default:
					return false;
			}
		}

		public static bool IsDisable(this AdvancedSceneLoadingMode mode)
		{
			switch (mode)
			{
				case AdvancedSceneLoadingMode.DisableAllOther:
				case AdvancedSceneLoadingMode.DisableLower:
				case AdvancedSceneLoadingMode.DisableLowerOrEqual:
					return true;
				default:
					return false;
			}
		}

		public static bool IsAdditional(this AdvancedSceneLoadingMode mode)
		{
			switch (mode)
			{
				case AdvancedSceneLoadingMode.Additional:
					return true;
				default:
					return false;
			}
		}

		public static IEnumerable<AdvancedScene> FindAffectedScenes(this AdvancedSceneLoadingMode mode, AdvancedScene newScene, IEnumerable<AdvancedScene> otherScenes)
		{
			switch (mode)
			{
				case AdvancedSceneLoadingMode.DisableAllOther:
				case AdvancedSceneLoadingMode.DestroyAllOther:
					return otherScenes;
				case AdvancedSceneLoadingMode.DisableLower:
				case AdvancedSceneLoadingMode.DestroyLower:
					return otherScenes.Where(s => s.Priority < newScene.Priority);
				case AdvancedSceneLoadingMode.DestroyLowerOrEqual:
				case AdvancedSceneLoadingMode.DisableLowerOrEqual:
					return otherScenes.Where(s => s.Priority <= newScene.Priority);
				default:
					return Enumerable.Empty<AdvancedScene>();
			}
		}
	}
}