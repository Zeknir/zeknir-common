﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zeknir.Logging;

namespace Zeknir.Common.Unity.SceneManagement
{
    public static class AdvancedSceneManager
    {
        private static HashSet<AdvancedScene> loadedScenes = new HashSet<AdvancedScene>();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void init()
        {
            loadedScenes.Add(new AdvancedScene(SceneManager.GetActiveScene(), 0));
        }

        public static CustomYieldInstruction LoadSceneAsync(string sceneName, int priority, AdvancedSceneLoadingMode loadingType, out AdvancedSceneReference sceneReference)
        {
            var buildIndex = SceneUtility.GetBuildIndexByScenePath(sceneName);
            if (buildIndex == -1)
                throw new ArgumentException($"Invalid scene: {sceneName} (Is it added to the Build Index?)", nameof(sceneName));

            return LoadSceneAsync(buildIndex, priority, loadingType, out sceneReference);
        }

        public static CustomYieldInstruction LoadSceneAsync(Scene scene, int priority, AdvancedSceneLoadingMode loadingType, out AdvancedSceneReference sceneReference)
        {
            if (scene.buildIndex == -1)
                throw new ArgumentException($"Invalid scene: {scene.name}", nameof(scene));
            
            return LoadSceneAsync(scene.buildIndex, priority, loadingType, out sceneReference);
        }

        public static CustomYieldInstruction LoadSceneAsync(int sceneIndex, int priority, AdvancedSceneLoadingMode loadingType, out AdvancedSceneReference sceneReference)
        {
            var advScene = new AdvancedScene(sceneIndex, priority);
            sceneReference = advScene;

            var affectedScenes = loadingType.FindAffectedScenes(advScene, loadedScenes).ToList();
            foreach (var affectedScene in affectedScenes)
            {
                if (loadingType.IsDestroy())
                {
                    loadedScenes.Remove(affectedScene);
                    SceneManager.UnloadSceneAsync(affectedScene.Scene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                }
                else if (loadingType.IsDisable())
                    advScene.DisableOtherScene(affectedScene);
            }

            loadedScenes.Add(advScene);
            var operation = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);
            advScene.InjectLoadingOperation(operation);

            var finishedLoading = false;
            operation.completed += o =>
            {
                SceneManager.SetActiveScene(advScene.Scene);
                finishedLoading = true;
            };

            return new WaitUntil(() => finishedLoading);
        }

        public static void UnloadAllLowerOrEqualScenesAsync(int priority)
        {
            var affectedScenes = loadedScenes.Where(s => s.Priority <= priority).ToList();
            foreach (var affectedScene in affectedScenes)
            {
                loadedScenes.Remove(affectedScene);
                SceneManager.UnloadSceneAsync(affectedScene.Scene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
        }

        public static CustomYieldInstruction UnloadSceneAsync(AdvancedSceneReference scene)
        {
            if (loadedScenes.Count <= 1)
            {
                Log.Write($"Can't unload only scene.", LogLevel.Error);
                return null;
            }

            var operation = SceneManager.UnloadSceneAsync(scene.Scene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

            var finishedLoading = false;
            operation.completed += o =>
            {
                var advScene = (AdvancedScene) scene;
                advScene.EnableAllOtherScenes();

                loadedScenes.Remove(advScene);

                if (SceneManager.GetActiveScene() == scene.Scene)
                {
                    var newActiveScene = loadedScenes.OrderByDescending(a => a.Priority).First();
                    SceneManager.SetActiveScene(newActiveScene.Scene);
                }

                finishedLoading = true;
            };

            return new WaitUntil(() => finishedLoading);
        }

        public static void Teardown(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }
    }
}