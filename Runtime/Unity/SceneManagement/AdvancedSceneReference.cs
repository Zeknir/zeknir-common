﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zeknir.Common.Unity.SceneManagement
{
	public abstract class AdvancedSceneReference
	{
		public Scene Scene { get; private set; }
		public int SceneIndex { get; private set; }
		public int Priority { get; private set; }

		public AdvancedSceneReference(int sceneIndex, int priority)
		{
			this.SceneIndex = sceneIndex;
			this.Priority = priority;
		}

		public AdvancedSceneReference(Scene scene, int priority) : this(scene.buildIndex, priority)
		{
			this.Scene = scene;
		}

		public void InjectLoadingOperation(AsyncOperation loadingOperation)
		{
			loadingOperation.completed += (o) => { this.Scene = SceneManager.GetSceneByBuildIndex(this.SceneIndex); };
		}
	}
}