﻿using UnityEngine;
using Zeknir.Common.Unity.Core;

namespace Zeknir.Common.Unity.Cameras
{
    [RequireComponent(typeof(Camera))]
    public class MainCameraSingletonComponent : SingletonComponent<MainCameraSingletonComponent>
    {
        public Camera Camera { get; private set; }

        private void Awake()
        {
            this.Camera = this.GetComponent<Camera>();
        }
    }
}