using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Zeknir.Logging;

namespace Zeknir.Common.Unity.Ui
{
    public delegate void SelectedTogglesChanged(List<ExtendedToggleComponent> selected);

    [Serializable]
    public class ToggleChangedEvent : UnityEvent<List<ExtendedToggleComponent>> { }

    public class ExtendedToggleGroupComponent : MonoBehaviour
    {
        public bool AllowSwitchOff = false;
        public bool AllowMultiSelect = false;

        public IEnumerable<ExtendedToggleComponent> SelectedToggles => this.selectedToggles;
        public List<ExtendedToggleComponent> AllToggles { get; private set; } = new List<ExtendedToggleComponent>();

        public event SelectedTogglesChanged SelectedToggleChanged;
        public ToggleChangedEvent SelectedToggleChangedEvent;

        private bool eventRunning;

        private readonly List<ExtendedToggleComponent> selectedToggles = new List<ExtendedToggleComponent>();

        public void RegisterToggle(ExtendedToggleComponent toggle)
        {
            if (toggle.Toggle.group != null)
            {
                Log.Write($"Can't register {toggle.gameObject} as it already contains a toggle group.", LogLevel.Error);
                return;
            }

            this.AllToggles.Add(toggle);
            this.OnToggleStateChange(toggle, toggle.Toggle.isOn);
        }

        public void DeregisterToggle(ExtendedToggleComponent toggle)
        {
            this.AllToggles.Remove(toggle);
            this.selectedToggles.Remove(toggle);
            this.changeToggleState(toggle, toggle.Toggle.isOn);
        }

        public void SetToggles(IEnumerable<ExtendedToggleComponent> togglesToSelect, bool doSilent = false)
        {
            var togglesList = togglesToSelect.ToList();
            if (!this.AllowMultiSelect && togglesList.Count > 1)
                return;

            var togglesDataList = togglesList.ToList();

            var deselectedToggles = this.selectedToggles.Except(togglesDataList);
            var newSelectedToggles = togglesDataList.Except(this.selectedToggles);

            foreach (var deselectedToggle in deselectedToggles)
                this.changeToggleState(deselectedToggle, false);

            foreach (var selectedToggle in newSelectedToggles)
                this.changeToggleState(selectedToggle, true);

            this.selectedToggles.Clear();
            this.selectedToggles.AddRange(togglesDataList);

            if (!doSilent)
                this.invokeSelectionChangedEvent(this.selectedToggles);
        }

        private void changeToggleState(ExtendedToggleComponent source, bool newState)
        {
            source.Toggle.SetIsOnWithoutNotify(newState);
            source.UpdateState(newState);
            this.UpdateToggle(source, newState);
        }

        protected virtual void UpdateToggle(ExtendedToggleComponent source, bool newState) { }

        public void OnToggleStateChange(ExtendedToggleComponent source, bool newState)
        {
            if (this.eventRunning)
                return;

            this.eventRunning = true;

            var isSelected = this.isSelected(source, out var selectedIndex);
            var tryMultiselect = this.AllowMultiSelect;

            if (newState && !isSelected)
            {
                if (!tryMultiselect)
                {
                    this.setAllBut(source, false);
                    this.selectedToggles.Clear();
                }

                this.changeToggleState(source, newState);
                this.selectedToggles.Add(source);
                this.invokeSelectionChangedEvent(this.selectedToggles);
            }
            else if (!newState && isSelected)
            {
                if (this.AllowSwitchOff || this.selectedToggles.Count > 1)
                {
                    if (tryMultiselect)
                        this.selectedToggles.RemoveAt(selectedIndex);
                    else if (this.selectedToggles.Count > 1)
                    {
                        newState = true;
                        this.setAllBut(source, false);
                        source.Toggle.SetIsOnWithoutNotify(true);
                        this.selectedToggles.Clear();
                        this.selectedToggles.Add(source);
                    }
                    else
                    {
                        this.setAllBut(source, false);
                        this.selectedToggles.Clear();
                    }

                    this.changeToggleState(source, newState);
                    this.invokeSelectionChangedEvent(this.selectedToggles);
                }
                else
                    source.Toggle.SetIsOnWithoutNotify(true);
            }

            this.eventRunning = false;
        }

        private void setAllBut(ExtendedToggleComponent exception, bool newValue)
        {
            for (var i = 0; i < this.AllToggles.Count; i++)
            {
                var toggle = this.AllToggles[i];

                if (toggle == exception)
                    continue;

                this.changeToggleState(toggle, newValue);
            }
        }

        private bool isSelected(ExtendedToggleComponent toggle, out int index)
        {
            index = this.selectedToggles.IndexOf(toggle);
            return index >= 0;
        }

        private void invokeSelectionChangedEvent(List<ExtendedToggleComponent> selected)
        {
            this.SelectedToggleChanged?.Invoke(selected);
            this.SelectedToggleChangedEvent.Invoke(selected);
        }
    }
}