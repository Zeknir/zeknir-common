using UnityEngine;
using UnityEngine.UI;

namespace Zeknir.Common.Unity.Ui
{
    [RequireComponent(typeof(Toggle))]
    public class ExtendedToggleComponent : MonoBehaviour
    {
        public ExtendedToggleGroupComponent Group;
        public Toggle Toggle { get; private set; }

        private void OnEnable()
        {
            this.Toggle = this.GetComponent<Toggle>();
            this.Toggle.onValueChanged.AddListener(this.handleToggleChanged);

            if (this.Group != null)
                this.Group.RegisterToggle(this);
        }

        public virtual void UpdateState(bool newState) { }

        private void handleToggleChanged(bool newState)
        {
            if (this.Group != null)
                this.Group.OnToggleStateChange(this, newState);
        }

        private void OnDisable()
        {
            this.Toggle.onValueChanged.RemoveListener(this.handleToggleChanged);
            this.Group.DeregisterToggle(this);
        }
    }
}