﻿using UnityEngine;

namespace Zeknir.Common.Unity.Ui
{
    public class TabExtendedToggleComponent : ExtendedToggleComponent
    {
        public GameObject TabPanel;

        public override void UpdateState(bool newState)
        {
            base.UpdateState(newState);

            if (this.TabPanel != null)
            {
                var showTab = this.isActiveAndEnabled && newState;
                this.TabPanel.SetActive(showTab);
            }
        }
    }
}