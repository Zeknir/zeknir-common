﻿using System.Collections;

namespace Zeknir.Common.Unity.Ui.Views
{
    public interface IAsyncView<TModel>
    {
        TModel Model { get; }
        IEnumerator SetModel(TModel newModel);
        IEnumerator RefreshView();
    }
}