﻿namespace Zeknir.Common.Unity.Ui.Views
{
    public interface IView<TModel>
    {
        TModel Model { get; set; }
        void RefreshView();
    }
}