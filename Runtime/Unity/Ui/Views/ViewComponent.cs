﻿using UnityEngine;

namespace Zeknir.Common.Unity.Ui.Views
{
    public abstract class ViewComponent<TModel> : MonoBehaviour, IView<TModel>
    {
        private TModel model;
        public TModel Model
        {
            get => this.model;
            set
            {
                var isNewModel = !Equals(this.model, value);

                if (isNewModel)
                    this.unsubscribeFromModel();

                this.model = value;

                if (isNewModel)
                    this.subscribeToModel();

                if (isNewModel)
                    this.OnNewModel();
                else
                    this.OnRefresh();
            }
        }

        private void HandleModelRefreshed()
        {
            this.RefreshView();
        }

        public void RefreshView()
        {
            this.OnRefresh();
        }

        protected virtual void OnNewModel()
        {
            this.OnRefresh();
        }

        protected abstract void OnRefresh();

        protected virtual void OnDestroy()
        {
            this.unsubscribeFromModel();
        }

        private void subscribeToModel()
        {
            if (this.model is IWatchableModel newWatchableModel)
                newWatchableModel.Refreshed += this.HandleModelRefreshed;
        }

        private void unsubscribeFromModel()
        {
            if (this.model is IWatchableModel oldWatchableModel)
                oldWatchableModel.Refreshed -= this.HandleModelRefreshed;
        }
    }
}