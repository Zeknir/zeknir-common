﻿using System;

namespace Zeknir.Common.Unity.Ui.Views
{
    public interface IWatchableModel
    {
        event Action Refreshed;
    }    
    
    public interface IRefreshableModel : IWatchableModel
    {
        void OnRefreshed();
    }
}