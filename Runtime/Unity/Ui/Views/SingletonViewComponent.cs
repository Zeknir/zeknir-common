﻿using Zeknir.Common.Unity.Core;

namespace Zeknir.Common.Unity.Ui.Views
{
    public abstract class SingletonViewComponent<TSelf, TModel> : SingletonComponent<TSelf>, IView<TModel> where TSelf : SingletonComponent<TSelf>
    {
        private TModel model;
        public TModel Model
        {
            get => this.model;
            set
            {
                var isNewModel = !Equals(this.model, value);

                var oldModel = this.model;
                this.model = value;

                if (isNewModel)
                    this.OnNewModel(oldModel);
                else
                    this.OnRefresh();
            }
        }

        public void RefreshView()
        {
            this.OnRefresh();
        }

        protected virtual void OnNewModel(TModel oldModel)
        {
            this.OnRefresh();
        }

        protected abstract void OnRefresh();
    }
}