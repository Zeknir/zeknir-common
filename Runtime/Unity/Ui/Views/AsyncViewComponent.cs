﻿using System.Collections;
using UnityEngine;

namespace Zeknir.Common.Unity.Ui.Views
{
    public abstract class AsyncViewComponent<TModel> : MonoBehaviour, IAsyncView<TModel>
    {
        public TModel Model { get; private set; }

        public IEnumerator SetModel(TModel newModel)
        {
            var isNewModel = !Equals(this.Model, newModel);

            this.Model = newModel;

            if (isNewModel)
                yield return this.OnNewModel();
            else
                yield return this.OnRefresh();
        }

        public IEnumerator RefreshView()
        {
            yield return this.OnRefresh();
        }

        protected virtual IEnumerator OnNewModel()
        {
            yield return this.OnRefresh();
        }

        protected abstract IEnumerator OnRefresh();
    }
}