﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zeknir.Common.Mathematics;
using Zeknir.Common.Unity.Cameras;

namespace Zeknir.Common.Unity.Ui
{
    public static class UiUtils
    {
        public static void ForceSelect(this GameObject gameObject)
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(gameObject);
        }

        public static Vector2 WorldSpaceToScreenSpace(this Canvas canvas, Vector2 worldSpace)
        {
            var canvasCam = canvas.worldCamera;
            if (!canvasCam)
                canvasCam = MainCameraSingletonComponent.Instance.Camera;

            return canvasCam.WorldToScreenPoint(worldSpace);
        }

        public static Vector2 TransformPointUi(this RectTransform rectTransform, Vector2 localUiPos)
        {
            var pivotOffset = (rectTransform.pivot - 0.5f.ToVector2()) * rectTransform.sizeDelta;
            var transformedPoint = localUiPos - pivotOffset;
            transformedPoint = rectTransform.TransformPoint(transformedPoint).ToVector2D();
            return transformedPoint;
        }

        public static Vector2 InverseTransformPointUi(this RectTransform rectTransform, Vector2 worldPos)
        {
            var pivotOffset = (rectTransform.pivot - 0.5f.ToVector2()) * rectTransform.sizeDelta;
            var transformedPoint = rectTransform.InverseTransformPoint(worldPos).ToVector2D();
            transformedPoint += pivotOffset;
            return transformedPoint;
        }

        public static Rect GetGlobalRect(this RectTransform rectTransform)
        {
            var localRect = rectTransform.rect;
            var rectOrigin = rectTransform.TransformPoint(localRect.min);
            var rectSize = rectTransform.TransformVector(localRect.size);
            return new Rect(rectOrigin, rectSize);
        }

        public static RectTransform GetRectTransform(this Component component)
        {
            return (RectTransform) component.transform;
        }

        public static RectTransform GetRectTransform(this GameObject gameObject)
        {
            return (RectTransform) gameObject.transform;
        }
    }
}