﻿using System.Collections.Generic;
using UnityEngine;

namespace Zeknir.Common.Unity.Ui.Components
{
    public class SyncableButtonGroup : MonoBehaviour
    {
        private Dictionary<SyncableButton, ButtonState> buttonStates = new Dictionary<SyncableButton, ButtonState>();

        private void HandleButtonStateChanged(SyncableButton sender, SyncableButton.StateChange value)
        {
            this.buttonStates[sender] = value.State;
            this.syncState(value.Instant);
        }

        private void syncState(bool instant)
        {
            var highestState = ButtonState.Normal;

            foreach (var buttonState in this.buttonStates)
            {
                if (buttonState.Value > highestState)
                    highestState = buttonState.Value;
            }

            foreach (var button in this.buttonStates.Keys)
                button.SyncState(highestState, instant);
        }

        public void Register(SyncableButton syncableButton)
        {
            this.buttonStates[syncableButton] = syncableButton.State;
            syncableButton.StateChanged += this.HandleButtonStateChanged;
        }

        public void Deregister(SyncableButton syncableButton)
        {
            this.buttonStates.Remove(syncableButton);
            syncableButton.StateChanged -= this.HandleButtonStateChanged;
        }
    }
}