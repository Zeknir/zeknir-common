﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Zeknir.Common.Unity.Ui
{
    public class BlockerUiComponent : MonoBehaviour, IPointerClickHandler
    {
        private Action callback;

        public void Block(Action onClickCallback)
        {
            this.callback = onClickCallback;
            this.gameObject.SetActive(true);
        }

        public void Unblock()
        {
            this.callback = null;
            if (this.gameObject != null)
                this.gameObject.SetActive(false);
        }

        public void HandleClick()
        {
            this.callback?.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            this.HandleClick();
        }

        private void OnDisable()
        {
            this.callback?.Invoke();
        }
    }
}