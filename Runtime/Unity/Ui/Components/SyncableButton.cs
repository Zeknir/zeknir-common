﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zeknir.Common.Core;

namespace Zeknir.Common.Unity.Ui.Components
{
    public class SyncableButton : Button
    {
        [Header("References")]
        public SyncableButtonGroup SyncGroup;

        public bool DoSync
        {
            get => this.doSync;
            set
            {
                this.doSync = value;
                this.updateSync();
            }
        }
        public ButtonState State { get; private set; }

        public event SimpleEventHandler<SyncableButton, StateChange> StateChanged;

        private bool isSyncing = false;
        private bool doSync;

        protected override void OnEnable()
        {
            this.updateSync();
        }

        protected override void OnDisable()
        {
            this.updateSync();
        }

        public void SyncState(ButtonState state, bool instant)
        {
            if (!this.interactable)
                return;

            this.isSyncing = true;
            this.DoStateTransition(ToSelectionState(state), instant);
            this.isSyncing = false;
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);

            if (!this.isSyncing)
            {
                this.State = ToButtonState(state);
                this.StateChanged?.Invoke(this, new StateChange(ToButtonState(state), instant));
            }
        }

        private void updateSync()
        {
            if (this.SyncGroup != null)
            {
                if (this.doSync && this.isActiveAndEnabled)
                    this.SyncGroup.Register(this);
                else
                    this.SyncGroup.Deregister(this);
            }
        }

        public readonly struct StateChange
        {
            public readonly ButtonState State;
            public readonly bool Instant;

            public StateChange(ButtonState state, bool instant)
            {
                this.State = state;
                this.Instant = instant;
            }
        }

        private static ButtonState ToButtonState(SelectionState state)
        {
            switch (state)
            {
                case SelectionState.Normal: return ButtonState.Normal;
                case SelectionState.Highlighted: return ButtonState.Highlighted;
                case SelectionState.Pressed: return ButtonState.Pressed;
                case SelectionState.Selected: return ButtonState.Selected;
                case SelectionState.Disabled: return ButtonState.Disabled;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private static SelectionState ToSelectionState(ButtonState state)
        {
            switch (state)
            {
                case ButtonState.Normal: return SelectionState.Normal;
                case ButtonState.Highlighted: return SelectionState.Highlighted;
                case ButtonState.Pressed: return SelectionState.Pressed;
                case ButtonState.Selected: return SelectionState.Selected;
                case ButtonState.Disabled: return SelectionState.Disabled;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }

    public enum ButtonState
    {
        Disabled,
        Normal,
        Highlighted,
        Pressed,
        Selected,
    }
}