﻿using UnityEngine;
using UnityEngine.UI;

namespace Zeknir.Common.Unity.Ui
{
	public class BarComponent : MonoBehaviour
	{
		public Image BarImage;

		public float Value
		{
			get => this.value;
			set
			{
				this.value = value;
				this.BarImage.fillAmount = value;
			}
		}

		[SerializeField]
		[Range(0f, 1f)]
		private float value;

		public void OnValidate()
		{
			this.BarImage.fillAmount = this.value;
		}
	}
}