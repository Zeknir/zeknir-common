﻿using UnityEngine;

namespace Zeknir.Common.Unity.Ui
{
	[RequireComponent(typeof(Animator))]
	public class SimplePopup : MonoBehaviour
	{
		private Animator animator;
		private bool alive;

		private void Awake()
		{
			this.animator = this.GetComponent<Animator>();
			this.alive = true;
			this.animator.SetBool("Alive", this.alive);
		}

		public void Close()
		{
			this.alive = false;
			this.animator.SetBool("Alive", this.alive);
		}
	}
}