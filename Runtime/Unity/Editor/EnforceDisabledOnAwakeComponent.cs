﻿using UnityEngine;

namespace Zeknir.Common.Unity.Editor
{
    [DefaultExecutionOrder(int.MinValue)]
    public class EnforceDisabledOnAwakeComponent : MonoBehaviour
    {
        [SerializeField]
        [HideInInspector]
        private bool wasEnforced = false;

        private void Awake()
        {
            if (!this.wasEnforced)
            {
                this.gameObject.SetActive(false);
                this.wasEnforced = true;
            }
        }
    }
}