﻿using UnityEngine;

namespace Zeknir.Common.Unity.Editor
{
    public class SortingLayerIdAttribute : PropertyAttribute { }
}