﻿using UnityEngine;

namespace Zeknir.Common.Unity.Assets
{
	[CreateAssetMenu(fileName = "Curve", menuName = "Custom/Curve", order = 1)]
	public class CurveAsset : ScriptableObject
	{
		public AnimationCurve Curve;

		public float EvaluateFloat(float value)
		{
			return this.Curve.Evaluate(value);
		}

		public int EvaluateInt(int value)
		{
			return Mathf.RoundToInt(this.Curve.Evaluate(value));
		}
	}
}