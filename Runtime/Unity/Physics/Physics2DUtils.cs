﻿using System.Collections.Generic;
using UnityEngine;

namespace Zeknir.Common.Unity.Physics
{
    public static class Physics2DUtils
    {
        private static List<RaycastHit2D> raycastHitCache = new List<RaycastHit2D>();

        public static RaycastHit2D BoxCastClosest(Vector2 origin, Vector2 size, float angle, Vector2 direction, ContactFilter2D contactFilter, float distance)
        {
            var hitCount = Physics2D.BoxCast(origin, size, angle, direction, contactFilter, raycastHitCache, distance);

            var minHit = new RaycastHit2D() {distance = float.PositiveInfinity};
            for (var i = 0; i < hitCount; i++)
            {
                var hit = raycastHitCache[i];
                if (hit.distance < minHit.distance)
                    minHit = hit;
            }

            return minHit;
        }
    }
}