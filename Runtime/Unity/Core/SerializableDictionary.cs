﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Zeknir.Common.Unity.Core
{
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<KeyValue> serializedItems = new List<KeyValue>();

        public void OnBeforeSerialize()
        {
            foreach (var item in this)
            {
                var existingIndex = this.serializedItems.FindIndex(a => Equals(a.Key, item.Key));
                if (existingIndex >= 0)
                {
                    this.serializedItems.RemoveAt(existingIndex);
                    this.serializedItems.Insert(existingIndex, new KeyValue(item.Key, item.Value));
                }
                else
                    this.serializedItems.Add(new KeyValue(item.Key, item.Value));
            }
        }

        public void OnAfterDeserialize()
        {
            this.Clear();

            for (var i = 0; i < this.serializedItems.Count; i++)
                this[this.serializedItems[i].Key] = this.serializedItems[i].Value;
        }

        [Serializable]
        private struct KeyValue
        {
            public TKey Key;
            public TValue Value;

            public KeyValue(TKey key, TValue value)
            {
                this.Key = key;
                this.Value = value;
            }
        }
    }
}