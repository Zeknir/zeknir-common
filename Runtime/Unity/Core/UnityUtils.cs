﻿using UnityEditor;

namespace Zeknir.Common.Unity.Core
{
    public class UnityUtils
    {
        public static void QuitApplication()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif
        }
    }
}