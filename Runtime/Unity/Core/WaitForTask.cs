﻿using System.Threading.Tasks;
using UnityEngine;

namespace Zeknir.Common.Unity.Core
{
	public class WaitForTask : CustomYieldInstruction
	{
		public override bool keepWaiting
		{
			get
			{
				if (this.Task.IsCompleted)
				{
					if (this.Task.Exception != null)
					{
						throw this.Task.Exception;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}
		}

		public readonly Task Task;

		public WaitForTask(Task task)
		{
			this.Task = task;
		}
	}
}