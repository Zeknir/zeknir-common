﻿using System;
using UnityEngine;

namespace Zeknir.Common.Unity.Core
{
    [Serializable]
    public class UnityTimer
    {
        [SerializeField]
        public float Interval;
        [SerializeField]
        public bool AutoRestart;
        [SerializeField]
        public bool UseUnscaledTime;

        public bool IsRunning = true;

        public bool HasRunOut => this.currentTimerValue >= this.Interval;

        private float currentTimerValue = 0f;
        private float lastTime = 0;

        private Func<float> timeSource;

        public UnityTimer(float interval, bool autoRestart = false)
        {
            this.Interval = interval;
            this.AutoRestart = autoRestart;
            this.timeSource = () => this.UseUnscaledTime ? Time.unscaledTime : Time.time;
            this.Restart();
        }

        public void SetCustomTimeSource(Func<float> timeSource)
        {
            this.timeSource = timeSource ?? throw new ArgumentNullException(nameof(timeSource));
            this.Restart();
        }

        public void Restart()
        {
            this.currentTimerValue = 0;
            this.lastTime = this.timeSource();
            this.IsRunning = true;
        }

        public bool Update()
        {
            var now = this.timeSource();
            var delta = now - this.lastTime;
            this.lastTime = now;
            return this.Update(delta);
        }

        public bool Update(float timeIncrease)
        {
            if (!this.IsRunning)
                return false;

            this.TryAutoRestart();

            this.currentTimerValue += timeIncrease;
            return this.HasRunOut;
        }

        private void TryAutoRestart()
        {
            if (this.AutoRestart)
            {
                if (this.HasRunOut)
                    this.currentTimerValue = Mathf.Max(0, this.currentTimerValue - this.Interval);
            }
        }
    }
}