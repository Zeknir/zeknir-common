using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityObject = UnityEngine.Object;

namespace Zeknir.Common.Unity.Core
{
    public static class UObjectUtils
    {
        public static T GetOrAddComponent<T>(this Component component) where T : Component
        {
            var target = component.GetComponent<T>();
            if (!target)
                target = component.gameObject.AddComponent<T>();
            return target;
        }

        public static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            var target = go.GetComponent<T>();
            if (!target)
                target = go.AddComponent<T>();
            return target;
        }

        public static T GetComponentInChildrenEx<T>(this Component component, bool includeParent = false, bool includeInactive = false)
        {
            return component.gameObject.GetComponentInChildrenEx<T>(includeParent, includeInactive);
        }

        public static T GetComponentInChildrenEx<T>(this GameObject go, bool includeParent = false, bool includeInactive = false)
        {
            return go.transform.GetComponentInChildrenEx<T>(includeParent, includeInactive);
        }

        public static T GetComponentInChildrenEx<T>(this Transform transform, bool includeParent = false, bool includeInactive = false)
        {
            if (includeParent)
                return transform.GetComponentInChildren<T>(includeInactive);

            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var result = child.GetComponentInChildren<T>(includeInactive);
                if (result != null)
                    return result;
            }

            return default;
        }

        public static List<T> FindObjectsOfType<T>(bool findInactive = false)
        {
            var allObjectArrays = GetAllRootSceneObjects();
            return allObjectArrays.SelectMany(a => a.GetComponentsInChildren<T>(findInactive)).ToList();
        }

        public static T FindObjectOfType<T>(bool findInactive = false) where T : UnityObject
        {
            var allObjectArrays = GetAllRootSceneObjects();
            return allObjectArrays.SelectMany(a => a.GetComponentsInChildren<T>(findInactive)).FirstOrDefault();
        }

        public static IEnumerable<GameObject> GetAllRootSceneObjects()
        {
            var allObjectArrays = new List<GameObject[]>();
            for (var i = 0; i < SceneManager.sceneCount; i++)
                allObjectArrays.Add(SceneManager.GetSceneAt(i).GetRootGameObjects());
            return allObjectArrays.SelectMany(a => a);
        }

        public static void DestroyAllChildren(this Transform transform, bool immediate = false, bool activeOnly = false)
        {
            var children = new List<GameObject>(transform.childCount);
            for (var i = 0; i < transform.childCount; i++)
                children.Add(transform.GetChild(i).gameObject);

            for (var i = 0; i < children.Count; i++)
            {
                var child = children[i];
                if (!activeOnly || child.activeSelf)
                    child.Destroy(immediate);
            }
        }

        public static void DestroyAndClear<T>(this List<T> list, bool immediate = false) where T : UnityObject
        {
            foreach (var item in list)
                item.Destroy(immediate);

            list.Clear();
        }

        public static void DestroyAndClearGameObjects<T>(this List<T> list, bool immediate = false) where T : Component
        {
            foreach (var item in list)
                item.gameObject.Destroy(immediate);

            list.Clear();
        }

        public static void DestroySafely(this UnityObject obj, bool isDestructionContext = false)
        {
#if UNITY_EDITOR
            if (isDestructionContext)
            {
                EditorApplication.delayCall += () =>
                {
                    if (obj)
                        UnityObject.DestroyImmediate(obj);
                };
            }
            else
                UnityObject.DestroyImmediate(obj);
#else
            UnityObject.Destroy(obj);
#endif
        }

        public static void Destroy(this UnityObject obj, bool immediate = false)
        {
            if (immediate)
                UnityObject.DestroyImmediate(obj);
            else
                UnityObject.Destroy(obj);
        }
    }
}