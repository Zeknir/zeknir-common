﻿using UnityEngine;
using Zeknir.Common.Mathematics;

namespace Zeknir.Common.Unity.Core
{
    public interface ISizedComponent2D
    {
        Vector2 ComponentSize { get; }
    }

    public static class SizedComponentExtension
    {
        public static Vector2 GetSize(this GameObject gameObject)
        {
            var size = gameObject.GetComponent<ISizedComponent2D>();
            return size?.ComponentSize ?? Vector2.zero;
        }

        public static GameObject SpawnSizedObject(GameObject prefab, Vector2 position)
        {
            var sizeYOffset = prefab.GetSize().y / 2f;
            return Object.Instantiate(prefab, position.ToVector3() + Vector3.up * sizeYOffset, Quaternion.identity);
        }
    }
}