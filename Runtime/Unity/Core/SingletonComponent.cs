﻿using UnityEngine;

namespace Zeknir.Common.Unity.Core
{
	public abstract class SingletonComponent<T> : MonoBehaviour where T : SingletonComponent<T>
	{
		private static T instance;
		public static T Instance
		{
			get
			{
				if (!instance)
				{
					instance = UObjectUtils.FindObjectOfType<T>(true);
				}

				return instance;
			}
		}
	}
}