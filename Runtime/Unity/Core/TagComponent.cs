﻿using System.Collections.Generic;
using UnityEngine;
using Zeknir.Common.Collections;
using Zeknir.Common.Core;

namespace Zeknir.Common.Unity.Core
{
    public abstract class TagComponent<T> : MonoBehaviour where T : TagComponent<T>
    {
        public string Tag;

        private void Awake()
        {
            Register(this as T);
        }

        private void OnDestroy()
        {
            Deregister(this as T);
        }

        private static Dictionary<string, HashSet<T>> data = new Dictionary<string, HashSet<T>>();

        public static event SimpleEventHandler<T> Registered;
        public static event SimpleEventHandler<T> Deregistered;

        public static void Register(T tagComponent)
        {
            if (!data.TryGetValue(tagComponent.Tag, out var set))
            {
                set = new HashSet<T>();
                data.Add(tagComponent.Tag, set);
            }

            set.Add(tagComponent);

            Registered?.Invoke(tagComponent);
        }

        public static ReadOnlySet<T> GetTaggedComponents(string tag)
        {
            if (data.TryGetValue(tag, out var set))
                return new ReadOnlySet<T>(set);
            else
                return new ReadOnlySet<T>(null);
        }

        public static void Deregister(T tagComponent)
        {
            if (!data.TryGetValue(tagComponent.Tag, out var set))
                return;

            set.Remove(tagComponent);

            Deregistered?.Invoke(tagComponent);
        }
    }
}