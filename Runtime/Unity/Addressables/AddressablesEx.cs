﻿#if ZEKNIR_ADDRESSABLES
using System;
using System.Reflection;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Zeknir.Common.Unity.Addressables
{
	public static class AddressablesEx
	{
		private static Type addressablesImplType;
		private static PropertyInfo addressablesImplField;
		private static MethodInfo trackHandleMethod;
		private static MethodInfo typelessProvideResourceMethod;

		static AddressablesEx()
		{
			addressablesImplField = typeof(UnityEngine.AddressableAssets.Addressables).GetProperty("Instance", BindingFlags.Static | BindingFlags.NonPublic);
			addressablesImplType = typeof(UnityEngine.AddressableAssets.Addressables).Assembly.GetType("UnityEngine.AddressableAssets.AddressablesImpl");
			trackHandleMethod = addressablesImplType.GetMethod("TrackHandle", BindingFlags.Instance | BindingFlags.NonPublic, null, new[] {typeof(AsyncOperationHandle)}, null);
			typelessProvideResourceMethod = typeof(ResourceManager).GetMethod("ProvideResource", BindingFlags.Instance | BindingFlags.NonPublic, null, new[] {typeof(IResourceLocation), typeof(Type)}, null);
		}

		public static AsyncOperationHandle LoadAssetDynamicallyAsync(IResourceLocation key, Type type = null)
		{
			var handle = (AsyncOperationHandle) typelessProvideResourceMethod.Invoke(UnityEngine.AddressableAssets.Addressables.ResourceManager, new object[] {key, type ?? key.ResourceType});
			var addressableImpl = addressablesImplField.GetValue(null);
			trackHandleMethod.Invoke(addressableImpl, new object[] {handle});
			return handle;
		}
	}
}
#endif