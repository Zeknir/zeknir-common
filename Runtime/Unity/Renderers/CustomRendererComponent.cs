﻿using UnityEngine;
using Zeknir.Common.Unity.Core;

namespace Zeknir.Common.Unity.Renderers
{
    [ExecuteAlways]
    public abstract class CustomRendererComponent : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField]
        private MeshRenderer meshRenderer;
        [HideInInspector]
        [SerializeField]
        private MeshFilter meshFilter;

        protected abstract void BakeMesh(Mesh mesh, MeshRenderer meshRenderer);

        private void OnEnable()
        {
            this.ensureComponent(ref this.meshRenderer);
            this.meshRenderer.enabled = true;

#if UNITY_EDITOR
            this.Bake();
#endif
        }

        private void OnDisable()
        {
            this.meshRenderer.enabled = false;
        }

        private void OnDestroy()
        {
            this.ensureComponent(ref this.meshRenderer);
            this.ensureComponent(ref this.meshFilter);

            if (this.meshRenderer)
                this.meshRenderer.DestroySafely(true);
            if (this.meshFilter)
                this.meshFilter.DestroySafely(true);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Application.isPlaying)
                return;

            this.Bake();
        }
#endif

        public void Bake()
        {
            this.ensureComponent(ref this.meshRenderer);
            this.ensureComponent(ref this.meshFilter);

            var mesh = this.meshFilter.sharedMesh;
            if (!mesh)
            {
                mesh = new Mesh();
                this.meshFilter.sharedMesh = mesh;
            }

            this.BakeMesh(mesh, this.meshRenderer);
        }

        private void ensureComponent<T>(ref T component) where T : Component
        {
            if (!component)
                component = this.getOrAddHiddenComponent<T>();
        }

        private T getOrAddHiddenComponent<T>() where T : Component
        {
            var com = this.GetOrAddComponent<T>();
            com.hideFlags = HideFlags.HideInInspector;
            return com;
        }
    }
}