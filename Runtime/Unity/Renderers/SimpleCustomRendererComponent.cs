﻿using UnityEngine;
using Zeknir.Common.Unity.Editor;

namespace Zeknir.Common.Unity.Renderers
{
    public abstract class SimpleCustomRendererComponent : CustomRendererComponent
    {
        [Header("Renderer Base")]
        public Material Material;
        [SortingLayerId]
        public int SortingLayer;
        public int SortingOrder;

        protected override void BakeMesh(Mesh mesh, MeshRenderer meshRenderer)
        {
            meshRenderer.sharedMaterial = this.Material;
            meshRenderer.sortingLayerID = this.SortingLayer;
            meshRenderer.sortingOrder = this.SortingOrder;
        }
    }
}