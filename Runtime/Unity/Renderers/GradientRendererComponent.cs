﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zeknir.Common.Mathematics;
using Zeknir.Common.Unity.Ui;

namespace Zeknir.Common.Unity.Renderers
{
    [RequireComponent(typeof(RectTransform))]
    public class GradientRendererComponent : SimpleCustomRendererComponent
    {
        public Gradient Gradient;
        public Direction4 Direction = Direction4.Right;

        protected override void BakeMesh(Mesh mesh, MeshRenderer meshRenderer)
        {
            base.BakeMesh(mesh, meshRenderer);

            var rect = this.GetRectTransform().rect;
            mesh.Clear();

            if (this.Gradient == null)
                this.Gradient = new Gradient();

            var colorKeys = this.Gradient.colorKeys;
            var alphaKeys = this.Gradient.alphaKeys;

            var keys = new Dictionary<float, Color>(colorKeys.Length);

            foreach (var colorKey in colorKeys)
                keys.Add(colorKey.time, this.Gradient.Evaluate(colorKey.time));

            foreach (var alphaKey in alphaKeys)
            {
                if (!keys.ContainsKey(alphaKey.time))
                    keys.Add(alphaKey.time, this.Gradient.Evaluate(alphaKey.time));
            }

            var sortedKeys = keys.OrderBy(a => a.Key).ToArray();

            var vertices = new Vector3[sortedKeys.Length * 2];
            var colors = new Color[sortedKeys.Length * 2];
            for (var i = 0; i < sortedKeys.Length; i++)
            {
                var verticesIndex = i * 2;
                var key = sortedKeys[i];

                var normPos1 = new Vector2(key.Key, 0);
                var normPos2 = new Vector2(key.Key, 1);

                if (!this.Direction.IsPositive())
                {
                    normPos1 = new Vector2(1 - normPos1.x, normPos1.y);
                    normPos2 = new Vector2(1 - normPos2.x, normPos2.y);
                }

                if (!this.Direction.IsHorizontal())
                {
                    normPos1 = normPos1.SwapAxis();
                    normPos2 = normPos2.SwapAxis();
                }

                var pos1 = new Vector2(normPos1.x * rect.width + rect.xMin, normPos1.y * rect.height + rect.yMin);
                var pos2 = new Vector2(normPos2.x * rect.width + rect.xMin, normPos2.y * rect.height + rect.yMin);

                vertices[verticesIndex] = pos1;
                vertices[verticesIndex + 1] = pos2;
                colors[verticesIndex] = key.Value.linear;
                colors[verticesIndex + 1] = key.Value.linear;
            }

            var trisCount = Mathf.Max(0, vertices.Length - 2);
            var tris = new int[trisCount * 3];
            for (var i = 0; i < trisCount / 2; i++)
            {
                var baseTrisIndex = i * 6;
                var baseVertexIndex = i * 2;

                tris[baseTrisIndex] = baseVertexIndex;
                tris[baseTrisIndex + 1] = baseVertexIndex + 1;
                tris[baseTrisIndex + 2] = baseVertexIndex + 2;

                tris[baseTrisIndex + 3] = baseVertexIndex + 1;
                tris[baseTrisIndex + 4] = baseVertexIndex + 3;
                tris[baseTrisIndex + 5] = baseVertexIndex + 2;
            }

            mesh.vertices = vertices;
            mesh.colors = colors;
            mesh.triangles = tris;
            mesh.RecalculateNormals();
        }
    }
}