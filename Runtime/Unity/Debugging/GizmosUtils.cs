﻿using UnityEngine;
using Zeknir.Common.Mathematics;

namespace Zeknir.Common.Unity.Debugging
{
    public static class GizmosUtils
    {
        public static void DrawArrow(Vector2 position, Vector2 direction, float arrowHeadSize = 0.1f)
        {
            Gizmos.DrawRay(position, direction);

            var endPos = position + direction;
            var negativeDirectionNormalized = -direction.normalized;

            var arrayRay1 = negativeDirectionNormalized.Rotate(22.5f) * arrowHeadSize;
            var arrayRay2 = negativeDirectionNormalized.Rotate(-22.5f) * arrowHeadSize;

            Gizmos.DrawRay(endPos, arrayRay1);
            Gizmos.DrawRay(endPos, arrayRay2);
        }

        public static void DrawPolygon(Vector2[] points, bool close = true)
        {
            var target = close ? points.Length : points.Length - 1;
            for (var i = 0; i < target; i++)
            {
                var p1 = points[i];
                var p2 = points[(i + 1) % points.Length];

                Gizmos.DrawLine(p1.ToVector3(), p2.ToVector3());
            }
        }
    }
}