﻿using System;
using System.Diagnostics;
using UnityEngine;
using Zeknir.Common.Mathematics;
using UDebug = UnityEngine.Debug;

namespace Zeknir.Common.Unity.Debugging
{
	public static class DebugUtils
	{
		#region Code

		public static void PreventStackOverflow(int maximumDepth = 1000)
		{
			if (new StackTrace().FrameCount > maximumDepth)
			{
				throw new StackOverflowException();
			}
		}

		#endregion


		#region Drawing

		public static void DrawBox(Vector2 center, Vector2 size, Color color, float duration = 0f, bool depthTest = true)
		{
			size /= 2f;
			var bottomLeft = new Vector2(center.x - size.x, center.y - size.y);
			var bottomRight = new Vector2(center.x + size.x, center.y - size.y);
			var topLeft = new Vector2(center.x - size.x, center.y + size.y);
			var topRight = new Vector2(center.x + size.x, center.y + size.y);

			UDebug.DrawLine(bottomLeft, topLeft, color, duration, depthTest);
			UDebug.DrawLine(topLeft, topRight, color, duration, depthTest);
			UDebug.DrawLine(topRight, bottomRight, color, duration, depthTest);
			UDebug.DrawLine(bottomRight, bottomLeft, color, duration, depthTest);
		}

		public static void DrawArrow(Vector2 position, Vector2 direction, Color color, float duration = 0f, bool depthTest = true, float arrowHeadSize = 0.1f)
		{
			UDebug.DrawRay(position, direction, color, duration, depthTest);

			var endPos = position + direction;
			var negativeDirectionNormalized = -direction.normalized;

			var arrayRay1 = negativeDirectionNormalized.Rotate(22.5f) * arrowHeadSize;
			var arrayRay2 = negativeDirectionNormalized.Rotate(-22.5f) * arrowHeadSize;

			UDebug.DrawRay(endPos, arrayRay1, color, duration, depthTest);
			UDebug.DrawRay(endPos, arrayRay2, color, duration, depthTest);
		}

		public static void DrawCross(Vector2 position, Color color, float duration = 0f, bool depthTest = true, float size = 0.1f)
		{
			size /= 2f;
			var bottomLeft = new Vector2(position.x - size, position.y - size);
			var bottomRight = new Vector2(position.x + size, position.y - size);
			var topLeft = new Vector2(position.x - size, position.y + size);
			var topRight = new Vector2(position.x + size, position.y + size);

			UDebug.DrawLine(bottomLeft, topRight, color, duration, depthTest);
			UDebug.DrawLine(bottomRight, topLeft, color, duration, depthTest);
		}

		public static void DrawCircle(Vector2 position, float radius, Color color, float duration = 0f, bool depthTest = true, float detail = 0.1f)
		{
			var circumference = 2 * radius * Mathf.PI;
			var lineCount = Mathf.Min(10, Mathf.RoundToInt(circumference / detail));

			for (var i = 0; i < lineCount; i++)
			{
				var ratioLineStart = i / (float) lineCount;
				var ratioLineEnd = (i + 1) / (float) lineCount;

				var lineStart = position + Vector2.up.Rotate(ratioLineStart * 360f) * radius;
				var lineEnd = position + Vector2.up.Rotate(ratioLineEnd * 360f) * radius;

				UDebug.DrawLine(lineStart, lineEnd, color, duration, depthTest);
			}
		}

		#endregion
	}
}