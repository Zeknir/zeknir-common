﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Zeknir.Common.Reflection
{
    public static class ReflectionUtils
    {
        private static List<Type> typesCache;
        
        public static IReadOnlyList<Type> GetAllTypes()
        {
            if (typesCache == null)
            {
                var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                typesCache = allAssemblies.SelectMany(a => a.GetTypes()).ToList();
            }

            return typesCache;
        }

        public static bool IsNullable(this Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }
        
        public static IEnumerable<Type> GetDerivedTypes(this Type baseType)
        {
            return GetAllTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t));
        }

        public static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                    return true;

                toCheck = toCheck.BaseType;
            }

            return false;
        }

        public static bool IsTypeAnyGenericOf(this Type toCheck, Type genericBase)
        {
            var toCheckBase = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
            return toCheckBase == genericBase;
        }

        public static string GetReadableName(this Type type)
        {
            var typeName = type.Name;

            if (type.IsGenericType)
            {
                var genericIndicator = typeName.LastIndexOf('`');
                typeName = typeName.Substring(0, genericIndicator);

                typeName += "<";
                var genericTypes = type.GenericTypeArguments.Select(t => t.GetReadableName());
                typeName += string.Join(", ", genericTypes);
                typeName += ">";
            }

            return typeName;
        }

        public static bool HasCustomAttribute<T>(this Type type) where T : Attribute
        {
            return type.GetCustomAttribute<T>() != null;
        } 
        
        public static bool HasCustomAttribute<T>(this MemberInfo member) where T : Attribute
        {
            return member.GetCustomAttribute<T>() != null;
        } 
        
        public static T GetCustomAttribute<T>(this Type type) where T : Attribute
        {
            return (T) Attribute.GetCustomAttribute(type, typeof(T));
        }

        public static T GetCustomAttribute<T>(this MemberInfo member) where T : Attribute
        {
            return (T) Attribute.GetCustomAttribute(member, typeof(T));
        }
    }
}